#include <iostream>

using namespace std;

template <typename Type1, typename Type2>
void print(const Type1 &first, const Type2 &second) {
  cout << first << " " << second << endl;
}

int main() {
  int number = 5;
  print("Svaret er ", number);
  print(100, 0.05);
  print('\n', "Hallo");
}

/* Kjøring av programmet:

Svaret er  5
100 0.05

 Hallo
*/
